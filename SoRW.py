"""
SoRW >>> Shutdown or Restart Wait
Shutdown or Restart your computer ::::
Coded by PyPI | 9/9/2018 | turkhackteam.org member: Sessizer
Bu küçük araç ile belirlediğiniz saniye sonrasında bilgisayarınızı kapatır veya yeniden başlatır.
"""

"""Additions"""
from tkinter import * #Tkinter modülü dahil edilir.
from tkinter import messagebox #Tkinter modülü içerisindeki messagebox dahil edilir. Kullanıcıya bilgi vermek için kullanılan kutucuk.
import subprocess #Cmd ekranına komut yazmak için kullanacağımız modül.
import time #Time modülü ile Gün/Ay/Yıl : Saat/Dakika/Saniye bilgileri alınır.

"""Class Area"""
class App(object):
    """Constructive Function"""
    def __init__(self):
        root.title("SoRW") #Pencere Başlığı
        root.geometry("300x250") #Pencere boyutu
        root.resizable(width=FALSE, height=FALSE) #Pencere boyutuyla oynanmaması için değerleri false yapıyoruz.
        root.iconbitmap("tools.ico") #Penceremize icon ekliyoruz.
        self.Panel() #Oluşturduğumuz Panel fonksiyonunu belirtiyoruz.
        self.Time() #Oluşturduğumuz Time fonksiyonunu belirtiyoruz

    """Main Area"""
    def Panel(self):
        """My Label"""
        self._myLabel = Label(root, #Bu labelın root ekranında olmasını sağlıyoruz.
                              text="Coded By PyPI", #Labelımıza yazı ekliyoruz.
                              font="Serif 10 italic", #Labelımızın fontunu değiştiriyoruz.
                              cursor="cross" #Labelın üzerine gelince fare imlecini değiştiriyoruz.

        )
        self._myLabel.place(relx=0.65, rely=0.9) #Labelın ekranda hangi bölgede olması gerektiğini belirtiyoruz.
        
        """Center Line Image"""
        self._line = PhotoImage(file="line.gif")
        self._ln = Label(image = self._line)
        self._ln.image = self._line
        self._ln.place(relx=-0.0, rely=0.25)

        """Button Frames Images-1"""
        self._buttonFrame = PhotoImage(file="buttonframe.png") #Butonların arkasında yer alan resim dosyasını yüklüyoruz
        self._bf = Label(image=self._buttonFrame)
        self._bf.image = self._buttonFrame
        self._bf.place(relx=0.12, rely=0.24)

        self._buttonFrame2 = PhotoImage(file="buttonframe.png")
        self._bf2 = Label(image=self._buttonFrame2)
        self._bf2.image = self._buttonFrame2
        self._bf2.place(relx=0.54, rely=0.24)

        """Shutdown Button"""
        self._buttonOne = Button(root,
                                 text="ShutDown",  #Buton Yazısını belirtiyoruz
                                 width=12,  #Butonun genişliğini belirtiyoruz
                                 bg="#181818", #Buton arkaplan rengini belirtiyoruz
                                 fg="lime", #Buton yazı rengini belirtiyoruz.
                                 cursor="hand2", # Fare imlecini değiştiriyoruz.
                                 overrelief="groove", # Buton üstüne gelince butona bir efekt uyguluyoruz.
                                 border=0,
                                 command=self.Research #Butona basıldıktan sonra yapması gerek komutu yazıyoruz.
        )
        self._buttonOne.place(relx=0.147, rely=0.27)

        """Cancel Button-1"""
        self._buttonTwo = Button(root,
                                 text="Cancel",
                                 width=12,
                                 bg="#181818",
                                 fg="red",
                                 cursor="hand2",
                                 overrelief="groove",
                                 border=0,
                                 command=self.ButtonOperation2
        )
        self._buttonTwo.place(relx=0.568, rely=0.27)

        """TextBox-1"""
        self._textBox = Text(root,
                             width=7,
                             height=1
        )
        self._textBox.place(relx=0.72, rely=0.1)

        """Label-1"""
        self._labelOne = Label(root,
                               text="Kapanmadan Önce\nBekleme Süresi (Sn)",
                               font="Arial 10 bold",
                               fg = "black"
        )
        self._labelOne.place(relx=0.2,rely=0.07)

        """Button Frames Images-2"""
        self._buttonFrame3 = PhotoImage(file="buttonframe.png")
        self._bf3 = Label(image=self._buttonFrame3)
        self._bf3.image = self._buttonFrame3
        self._bf3.place(relx=0.12, rely=0.7)

        self._buttonFrame4 = PhotoImage(file="buttonframe.png")
        self._bf4 = Label(image=self._buttonFrame4)
        self._bf4.image = self._buttonFrame4
        self._bf4.place(relx=0.54, rely=0.7)

        """Restart Button"""
        self._buttonThree = Button(root,
                                 text="Restart",
                                 width=12,
                                 bg="#181818",
                                 fg="cyan",
                                 cursor="hand2",
                                 overrelief="groove",
                                 border=0,
                                 command=self.ButtonOperation3
        )
        self._buttonThree.place(relx=0.146, rely=0.73)

        """Cancel Button-2"""
        self._buttonFour = Button(root,
                                 text="Cancel",
                                 width=12,
                                 bg="#181818",
                                 fg="red",
                                 cursor="hand2",
                                 overrelief="groove",
                                 border=0,
                                 command=self.ButtonOperation4
        )
        self._buttonFour.place(relx=0.566, rely=0.73)

        """TextBox-2"""
        self._textBox2 = Text(root,
                             width=7,
                             height=1,
        )
        self._textBox2.place(relx=0.72, rely=0.53)

        """Label-2"""
        self._labelTwo = Label(root,
                               text="Yeniden Başlatmadan Önce\n Bekleme Süresi (Sn)",
                               font="Arial 10 bold",
                               fg="black"
        )
        self._labelTwo.place(relx=0.10, rely=0.5)

    def Research (self):
        self.inf = self._textBox.get("1.0", END)
        print(self.inf.isalpha())
        ###Düzenlenecek
        
    """Time Area"""
    def Time(self):
        self._time = time.localtime() #Time modülü içerisindeki localtime() fonksiyonunu çağırıyoruz.
        self._year = str(self._time[0]) #Bu fonksiyon içerisindeki bilgileri göstermemize yarıyor.
        self._month = str(self._time[1])
        self._day = str(self._time[2])
        self._hour = str(self._time[3])
        self._min = str(self._time[4])
        self._second = str(self._time[5])

    """Operation Area"""
    def ButtonOperation1(self):
        self.info = messagebox.showinfo("Uyarı",
                                        "Bilgisayarınız " + self.inf + "saniye sonra kapanacaktır.",
                                        detail=
                                        self._day + '/' +
                                        self._month + '/' +
                                        self._year + ' | ' +
                                        self._hour + ':' +
                                        self._min + ':' +
                                        self._second
        )
        self.cmd1 = subprocess.Popen('cmd.exe /K shutdown -s -t {} & exit'.format(self.inf)) #Cmd ekranına komut yazdırıyoruz.

    def ButtonOperation2(self):
        self.cmd1 = subprocess.Popen('cmd.exe /K shutdown -a')
        self.info = messagebox.showinfo("Bilgi",
                                        "İşlem İptal Edilmiştir.",
                                        detail="İptal Edilme Tarihi: " +
                                        self._day + '/' +
                                        self._month + '/' +
                                        self._year + ' | ' +
                                        self._hour + ':' +
                                        self._min + ':' +
                                        self._second
        )

    def ButtonOperation3(self):
        self.inf = self._textBox2.get("1.0", END)
        self.info = messagebox.showinfo("Uyarı",
                                        "Bilgisayarınız " + self.inf + "saniye içinde yeniden başlatılacaktır.",
                                        detail=
                                        self._day + '/' +
                                        self._month + '/' +
                                        self._year + ' | ' +
                                        self._hour + ':' +
                                        self._min + ':' +
                                        self._second
        )
        self.cmd1 = subprocess.Popen('cmd.exe /K shutdown -r -t '+ self.inf )

    def ButtonOperation4(self):
        self.cmd1 = subprocess.Popen('cmd.exe /K shutdown -a')
        self.info = messagebox.showinfo("Bilgi",
                                        "Yeniden Başlatma İşlemi İptal Edildi.",
                                        detail="İptal Edilme Tarihi: " +
                                               self._day + '/' +
                                               self._month + '/' +
                                               self._year + ' | ' +
                                               self._hour + ':' +
                                               self._min + ':' +
                                               self._second
        )

""""İdentifiers"""
root = Tk()
App()
root.attributes('-alpha', 0.9) #Pencereye saydamlık özelliği veriyoruz.
root.mainloop() #Pencerenin sürekli açık kalmasını sağlıyoruz.
